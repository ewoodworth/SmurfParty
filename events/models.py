from __future__ import unicode_literals
from django.db import models
from django.utils import timezone
from django.utils.encoding import python_2_unicode_compatible

import datetime


@python_2_unicode_compatible
class Question(models.Model):
    question_text = models.CharField(max_length=200)
    pub_date = models.DateTimeField('date published')

    def was_published_recently(self):
        return self.pub_date <= timezone.now() - datetime.timedelta(days=1)

    def __str__(self):
        return self.question_text


@python_2_unicode_compatible
class Choice(models.Model):
    question = models.ForeignKey(Question, on_delete=models.CASCADE)
    choice_text = models.CharField(max_length=200)
    votes = models.IntegerField(default=0)

    def __str__(self):
        return self.choice_text


@python_2_unicode_compatible
class Event(models.Model):
    title = models.CharField(max_length=200)
    date = models.DateTimeField
    time_start = models.DateTimeField
    time_end = models.DateTimeField

    def __str__(self):
        return self.title

@python_2_unicode_compatible
class User(models.Model):
    username = models.CharField(max_length=200)
    first_name = models.CharField(max_length=200)
    email = models.CharField(max_length=200)

    def __str__(self):
        return self.email